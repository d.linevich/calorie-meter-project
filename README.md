# Calorie Meter
**Calorie Meter** - приложение, которое поможет вам похудеть! Вы сможете следить за использованными калориями и не давать себе переедать. 
*Удачи в ваших полезных начинаниях!*

## Разработчики
**Линевич Дмитрий Александрович** -> *d.linevich@g.nsu.ru*

**Щеголева Ангелина Ивановна** -> *a.shshegoleva1@g.nsu.ru*